This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm run start-all`

Please, run `npm i` before this script 🤞🏻

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

This script also starts JSON server

Please, note that due to the reason I use JSON server I fetch some of the data in the very beginning
and do not use urls with get parameters, in few words, I am trying to minimize the number of request.

**The data is not persisted.**

Tests will be added later, together with pipelines setup.

Thanks! :)