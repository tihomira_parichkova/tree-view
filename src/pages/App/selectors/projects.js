export const getProjects = ({ projects: allProjects = [] }) => allProjects

export const getCompanyProjects = (projects, id) => projects.filter((project) => project.companyId === id)

export const getEmployeeProjects = (projects, id) => {
    debugger;
    return projects.filter((project) => project.employeesId?.includes(id))
}