export const getEmployees = ({ employees }) => employees;

export const getCompanyEmployees = (employees, companyId) => employees.filter((employee) => employee.companyId === companyId);
