export const getCompanyAddresses = ({ companyAddresses }) => {
    return companyAddresses;
}
export const getAddressPerCompany = (addresses, id) => {
    return addresses.filter((address) => address.companyId === id)
}