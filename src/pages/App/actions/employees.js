export const getEmployeesAction = 'GET_EMPLOYEES';
export const successfullyLoadEmployees = 'SUCCESSFULLY_LOADED_EMPLOYEES';
export const failureLoadEmployees = 'FAILURE_LOADING_EMPLOYEES';

export const loadEmployees = () => {
    return function action (dispatch) {
        fetch('http://localhost:3001/employees')
            .then(async (res) => {
                return res.json().then(
                    response => dispatch(successLoadingEmployees(response)),
                    err => dispatch(failureLoadingEmployees(err))
                );
            })
            .catch((err) => {
                console.log(err);
            })
    }
};

export const successLoadingEmployees = (result) => ({
    type: successfullyLoadEmployees,
    payload: result,
})

export const failureLoadingEmployees = (_err) => ({
    type: successfullyLoadEmployees,
    payload: [],
})