export const getProjectsAction = 'GET_PROJECTS';
export const successfullyLoadProjects = 'SUCCESSFULLY_LOAD_PROJECTS';
export const failureLoadProjects = 'FAILURE_LOAD_PROJECTS';
export const changeName = 'CHANGE_PROJECT_NAME';
export const unassignEmployee = 'UNASSIGN_EMPLOYEE';
export const assignEmployee = 'ASSIGN_EMPLOYEE';

export const loadProjects = () => {
    return function action (dispatch) {
        fetch('http://localhost:3001/projects')
            .then(async (res) => {
                return res.json().then(
                    response => dispatch(successLoadingProjects(response)),
                    err => dispatch(failureLoadingProjects(err))
                );
            })
            .catch((err) => {
                console.log(err);
            })
    }
};

export const successLoadingProjects = (result) => ({
    type: successfullyLoadProjects,
    payload: result,
})

export const failureLoadingProjects = (_err) => ({
    type: successfullyLoadProjects,
    payload: []
})

export const changeProjectName = (newName, projectId) => ({
    type: changeName,
    payload: {
        id: projectId,
        name: newName
    }
})

export const unassignEmployeeFromProject = (employeeId, projectId) => ({
    type: unassignEmployee,
    payload: {
        id: projectId,
        employeeId: employeeId
    }
});

export const assignEmployeeToProject = (employeeId, projectId) => ({
    type: assignEmployee,
    payload: {
        id: projectId,
        employeeId: employeeId
    }
});