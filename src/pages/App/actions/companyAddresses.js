export const getcompanyAddressesAction = 'GET_COMPANY_ADRESSES';
export const successfullyLoadcompanyAddresses = 'SUCCESSFULLY_LOADED_COMPANY_ADRESSES';
export const failureLoadcompanyAddresses = 'FAILURE_LOADING_COMPANY_ADRESSES';

export const loadcompanyAddresses = () => {
    return function action (dispatch) {
        fetch('http://localhost:3001/company-addresses')
            .then(async (res) => {
                return res.json().then(
                    response => dispatch(successLoadingcompanyAddresses(response)),
                    err => dispatch(failureLoadingcompanyAddresses(err))
                );
            })
            .catch((err) => {
                console.log(err);
            })
    }
};

export const successLoadingcompanyAddresses = (result) => ({
    type: successfullyLoadcompanyAddresses,
    payload: result,
})

export const failureLoadingcompanyAddresses = (_err) => ({
    type: successfullyLoadcompanyAddresses,
    payload: [],
})