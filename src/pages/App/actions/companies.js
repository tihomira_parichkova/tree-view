export const getCompaniesAction = 'GET_COMPANIES';
export const successfullyLoadCompanies = 'SUCCESSFULLY_LOADED_COMPANIES';
export const failureLoadCompanies = 'FAILURE_LOADING_COMPANIES';

export const loadCompanies = () => {
    return function action (dispatch) {
        fetch('http://localhost:3001/companies')
            .then(async (res) => {
                return res.json().then(
                    response => dispatch(successLoadingCompanies(response)),
                    err => dispatch(failureLoadingCompanies(err))
                );
            })
            .catch((err) => {
                console.log(err);
            })
    }
};

export const successLoadingCompanies = (result) => ({
    type: successfullyLoadCompanies,
    payload: result,
})

export const failureLoadingCompanies = (_err) => ({
    type: successfullyLoadCompanies,
    payload: [],
})