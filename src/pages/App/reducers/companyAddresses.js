import {
    getcompanyAddressesAction,
    successfullyLoadcompanyAddresses,
    failureLoadcompanyAddresses
} from '../actions/companyAddresses';

export const companyAddressesReducer = (state = [], action) => {
    switch (action.type) {
        case getcompanyAddressesAction:
            return [...state];
        case successfullyLoadcompanyAddresses:
            return [...state, ...action.payload];

        case failureLoadcompanyAddresses:
            return [...state];

        default:
            return state;
    }
};
