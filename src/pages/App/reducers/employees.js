import { getEmployeesAction, successfullyLoadEmployees, failureLoadEmployees } from '../actions/employees';

export const employeesReducer = (state = [], action) => {
    switch (action.type) {
        case getEmployeesAction:
            return [...state];

        case successfullyLoadEmployees:
            return [...state, ...action.payload];

        case failureLoadEmployees:
            return [...state];

        default:
            return state
    }
};
