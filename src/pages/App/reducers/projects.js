import {
    getProjectsAction,
    successfullyLoadProjects,
    failureLoadProjects,
    changeName,
    unassignEmployee,
    assignEmployee
} from '../actions/projects';

export const projectsReducer = (state = [], action) => {
    switch (action.type) {
        case getProjectsAction:
            return [...state];
        case successfullyLoadProjects:
            return [...state, ...action.payload];
        case failureLoadProjects:
            return [...state];
        case changeName: {
            const index = state.findIndex((project) => project.id === action.payload.id );

            state[index] = {
                ...state[index],
                name: action.payload.name
            };

            return [...state]
        }
        case unassignEmployee: {
            const index = state.findIndex((project) => project.id === action.payload.id );

            state[index] = {
                ...state[index],
                employeesId: state[index].employeesId.filter((id) => id !== action.payload.employeeId)
            }

            return [...state]
        }
        case assignEmployee: {
            debugger;
            const index = state.findIndex((project) => project.id === action.payload.id );

            state[index] = {
                ...state[index],
                employeesId: [...state[index].employeesId, action.payload.employeeId]
            }

            return [...state]
        }
        default: {
            return state
        }
    }
};
