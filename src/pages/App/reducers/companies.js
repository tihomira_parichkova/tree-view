import { getCompaniesAction, successfullyLoadCompanies, failureLoadCompanies } from '../actions/companies';

export const companiesReducer = (state = [], action) => {
    switch (action.type) {
        case getCompaniesAction:
            return [...state];
        case successfullyLoadCompanies:
            return [
                ...state,
                ...action.payload,
            ]
        case failureLoadCompanies:
            return [...state];
        default:
            return state;
    }
};
