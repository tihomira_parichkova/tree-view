export const getEmployeesByJobArea = (employees) => (
    employees.reduce((acc, employee) => {
        const { jobArea } = employee;

        if (!acc[jobArea]) {
            acc[jobArea] = [];
        }

        acc[jobArea] = [
            ...acc[jobArea],
            {
                ...employee,
            }
        ];

        return acc;
    }, {})
)
