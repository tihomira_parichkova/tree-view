import React, { useState } from 'react';
import { PropTypes } from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { TransitionsModal as Modal } from './../../../components/Modal';
import {
    TextField,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Button,
    Box
} from '@material-ui/core';
import {
    changeProjectName,
    unassignEmployeeFromProject,
    assignEmployeeToProject
} from './../actions/projects';
import { getCompanyEmployees, getEmployees } from './../selectors/employees';

export const ProjectContainer = ({ id, name, employeeId, companyId }) => {
    const dispatch = useDispatch();
    const employees = useSelector(getEmployees);
    const companyEmployees = getCompanyEmployees(employees, companyId);
    const [open, setOpen] = useState(false);
    const [newName, setNewName] = useState('');
    const [selectedEmployee, setSelectedEmployee] = useState('');

    const updateProjectName = (newValue, projectId) => {
        setOpen(false);
        dispatch(changeProjectName(newValue, projectId))
    };

    const unassignEmployee = (empId, projectId) => {
        setOpen(false);
        dispatch(unassignEmployeeFromProject(empId, projectId))
    };

    const assignEmployee = (empId, projectId) => {
        setOpen(false);
        dispatch(assignEmployeeToProject(empId, projectId))
    };

    return (
        <>
            <div onClick={() => setOpen(!open)}>
                {name}
            </div>

            <Modal open={open} onClose={() => { setOpen(!open) }}>
                <>
                    <p>Manage projects</p>
                    <hr />
                    <Box display='flex' justifyContent='space-between' alignItems='center'>
                        <TextField
                            style={{ margin: '10px', width: '250px' }}
                            label="Old name"
                            variant="outlined"
                            value={name}
                            readOnly
                            disabled
                        />
                        <TextField
                            style={{ margin: '10px', width: '250px' }}
                            value={newName}
                            onChange={(e) => setNewName(e.target.value)}
                            label="New name"
                            variant="outlined"
                            readOnly
                        />
                        <Button
                            style={{ margin: '10px' }}
                            variant="contained"
                            color="primary"
                            onClick={() => { updateProjectName(newName, id) }}
                        >
                            Set new name
                        </Button>
                    </Box>
                    <p>Manage employees</p>
                    <hr />
                    <Box display='flex' justifyContent='space-between' alignItems='center'>
                        <FormControl variant="outlined" style={{ margin: '10px', width: '250px' }}>
                            <InputLabel id="employees-selector">Employees</InputLabel>
                            <Select
                                labelId="employees-selector"
                                id="demo-simple-select-outlined"
                                value={selectedEmployee}
                                onChange={(e) => setSelectedEmployee(e.target.value)}
                                label="Employee"
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                {companyEmployees.map((employee) => (
                                    <MenuItem
                                        key={employee.id}
                                        value={employee.id}
                                    >
                                        {`${employee.firstName} ${employee.lastName}`}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                        <Button
                            style={{ margin: '10px' }}
                            variant="contained"
                            color="primary"
                            onClick={() => { assignEmployee(selectedEmployee, id) }}
                        >
                            Assign employee to current project
                        </Button>
                    </Box>
                    <p>Unassign employee</p>
                    <hr />
                    <Box display='flex' justifyContent='center' alignItems='center'>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={() => { unassignEmployee(employeeId, id) }}>
                            Unassign employee from current project
                        </Button>
                    </Box>
                </>
            </Modal>
        </>
    )
}

ProjectContainer.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    employeeId: PropTypes.string,
    companyId: PropTypes.string
}