import React, { useState } from 'react';
import { PropTypes } from 'prop-types';
import { getEmployeeProjects } from '../selectors/projects';
import { Employee } from './../../../components/Employee';

export const EmployeeContainer = ({
    id,
    companyId,
    projects,
    firstName,
    lastName,
    dateOfBirth,
    jobTitle,
    jobType
}) => {
    const [shouldShow, setShouldShow] = useState(false);

    const formatDate = (dateUTCString) => new Intl.DateTimeFormat().format(new Date(dateUTCString));

    return (
        <>
            <Employee
                id={id}
                companyId={companyId}
                fullName={`${firstName} ${lastName}`}
                dateOfBirth={formatDate(dateOfBirth)}
                jobTitle={jobTitle}
                jobType={jobType}
                assignedProjects={getEmployeeProjects(projects, id)}
                show={shouldShow}
                eventHandler={() => setShouldShow(!shouldShow)}
            />
        </>
    )
}

EmployeeContainer.propTypes = {
    id: PropTypes.string,
    companyId: PropTypes.string,
    projects: PropTypes.array,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    dateOfBirth: PropTypes.string,
    jobTitle: PropTypes.string,
    jobType: PropTypes.string
}