import React, { useState } from 'react';
import { PropTypes } from 'prop-types';
import { Company } from './../../../components/Company';
import { getEmployeesByJobArea } from './../utils';

export const CompanyContainer = ({ id, name, projects, employees, address }) => {
    const [showMore, setShowMore] = useState(false);
    const employeesByJobArea = getEmployeesByJobArea(employees);

    const formatCompanyAdress = (companyAddress) => {
        return companyAddress.map((addr) => {
            return `${addr.street}, ${addr.city} ${addr.country}, ${addr.state}`;
        }).join(' ; ')
    }

    return (
        <>
            <Company
                id={id}
                name={name}
                employeesPerArea={employeesByJobArea}
                address={formatCompanyAdress(address)}
                projects={projects}
                showMore={showMore}
                eventHandler={() => setShowMore(!showMore)}
            />
        </>
    )
}

CompanyContainer.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    projects: PropTypes.array,
    employees: PropTypes.array,
    address: PropTypes.array
}