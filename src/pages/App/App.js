import React, { useEffect } from 'react';
import { Typography } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { loadCompanies } from './actions/companies';
import { loadcompanyAddresses } from './actions/companyAddresses';
import { loadEmployees } from './actions/employees';
import { loadProjects } from './actions/projects';

import { getCompanies } from './selectors/companies';
import { getCompanyAddresses, getAddressPerCompany } from './selectors/companyAddresses';
import { getEmployees, getCompanyEmployees } from './selectors/employees';
import { getCompanyProjects, getProjects } from './selectors/projects';
import { CompanyContainer } from './components/CompanyContainer';

export const App = (...args) => {
  console.log(args);
  const dispatch = useDispatch();

  const companies = useSelector(getCompanies);
  const projects = useSelector(getProjects);
  const employees = useSelector(getEmployees);
  const companyAddresses = useSelector(getCompanyAddresses);

  // TODO: I would not fetch everything in a real project :)
  useEffect(() => {
    dispatch(loadCompanies());
    dispatch(loadEmployees());
    dispatch(loadProjects());
    dispatch(loadcompanyAddresses());
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        Tick 42 - Pre-interview
      </header>
      <section className="main">
        <Typography type="heading">Companies</Typography>
        <div>
          {
            companies.map((company) => {
              return (
                <CompanyContainer
                  key={company.id}
                  id={company.id}
                  name={company.name}
                  projects={getCompanyProjects(projects, company.id)}
                  employees={getCompanyEmployees(employees, company.id)}
                  address={getAddressPerCompany(companyAddresses, company.id)}
                />
              )
            })
          }
        </div>
      </section>
    </div>
  );
}