import { combineReducers } from 'redux';
import { companiesReducer } from '../pages/App/reducers/companies';
import { companyAddressesReducer } from '../pages/App/reducers/companyAddresses';
import { employeesReducer } from '../pages/App/reducers/employees';
import { projectsReducer } from '../pages/App/reducers/projects';

const reducer = combineReducers({
   companies: companiesReducer,
   companyAddresses: companyAddressesReducer,
   employees: employeesReducer,
   projects: projectsReducer
});

export default reducer;
