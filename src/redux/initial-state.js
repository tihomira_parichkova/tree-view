export const initialState = {
    loading: false,
    error: false,
    companies: [],
    companyAddresses: [],
    employees: [],
    projects: []
};
