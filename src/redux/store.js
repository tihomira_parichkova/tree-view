import { createStore, applyMiddleware, compose } from 'redux';
import middleware from './middleware';
import rootReducer from './reducers';


export const store = (state) => {
    const appliedMiddleware = applyMiddleware(...middleware);
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    return createStore(
        rootReducer,
        state,
        composeEnhancers(
            appliedMiddleware
        )
    );
}
