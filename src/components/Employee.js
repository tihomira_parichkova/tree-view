import React from 'react';
import { PropTypes } from 'prop-types';
import { Box } from '@material-ui/core';
import { Toggle } from './common/Toggle';
import { AdditionalInfo } from './common/Additionalnformation';
import { ProjectContainer } from './../pages/App/components/ProjectContainer';

export const Employee = ({
    id,
    companyId,
    fullName,
    dateOfBirth,
    jobTitle,
    jobType,
    assignedProjects,
    show,
    eventHandler
}) => {
    const anyProjects = assignedProjects && assignedProjects.length > 0;

    return (
        <>
            <Toggle show={show} eventHandler={() => eventHandler()}>
                {fullName}
            </Toggle>
            {show &&
                (
                    <Box display='flex' >
                        <div style={{ marginLeft: '1.5rem' }}>
                            <AdditionalInfo field="Date of Birth" value={dateOfBirth} />
                            <AdditionalInfo field="Job Title" value={jobTitle} />
                            <AdditionalInfo field="Type of Job" value={jobType} />
                        </div>
                        <div style={{ marginLeft: '1rem' }}>
                            <AdditionalInfo field="Assigned projects" />
                            {anyProjects ?
                                assignedProjects.map((project) => (
                                    <ProjectContainer
                                        id={project.id}
                                        companyId={companyId}
                                        key={project.id}
                                        name={project.name}
                                        employeeId={id}
                                    />
                                ))
                                : <div> No projects assigned yet. </div>
                            }
                        </div>
                    </Box>
                )}
        </>
    )
}

Employee.propTypes = {
    id: PropTypes.string,
    companyId: PropTypes.string,
    fullName: PropTypes.string,
    dateOfBirth: PropTypes.string,
    jobTitle: PropTypes.string,
    jobType: PropTypes.string,
    assignedProjects: PropTypes.array,
    show: PropTypes.bool,
    eventHandler: PropTypes.func
};
