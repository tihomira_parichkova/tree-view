import React, { useState } from 'react';
import { PropTypes } from 'prop-types';
import { AdditionalInfo } from './common/Additionalnformation';

export const JobArea = ({
    jobArea,
    jobAreaEmployeesCount,
    projectsCount
}) => {
    const [showInfo, setShowInfo] = useState(false);

    return (
        <div
            style={{ marginBottom: '1rem', marginTop: '1rem' }}
            onClick={() => { setShowInfo(!showInfo) }}
        >
            <AdditionalInfo field="Job area" value={jobArea} />
            {showInfo && (
                <div style={{ marginBottom: '.5rem', marginTop: '.5rem' }}>
                    <AdditionalInfo field="Employees count" value={jobAreaEmployeesCount} />
                    <AdditionalInfo field="Projects count" value={projectsCount} />
                </div>
            )}
        </div>
    )
}

JobArea.propTypes = {
    jobArea: PropTypes.string,
    jobAreaEmployeesCount: PropTypes.number,
    projectsCount: PropTypes.number
}
