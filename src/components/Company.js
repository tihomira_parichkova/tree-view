import React from 'react';
import { PropTypes } from 'prop-types';
import { Box } from '@material-ui/core';
import { Toggle } from './common/Toggle';
import { AdditionalInfo } from './common/Additionalnformation';
import { EmployeeContainer } from './../pages/App/components/EmployeesContainer';
import { JobArea } from './JobArea';

export const Company = ({ id, name, address, projects, employeesPerArea, showMore, eventHandler }) => (
    <div style={{ marginTop: '1rem', marginBottom: '1rem' }}>
        <Toggle show={showMore} eventHandler={() => { eventHandler() }}>
            <div>
                {name}
                <div style={{ marginLeft: '3rem' }}>
                    {showMore && (
                        <>
                            <AdditionalInfo
                                field="Address"
                                value={address}
                            />
                            <AdditionalInfo field="Projects" />
                            {projects.map((project) => (<div key={project.id}>{project.name}</div>))}
                        </>
                    )}
                </div>
            </div>
        </Toggle>
        <Box display='flex' alignItems='flex-start'>
            <div style={{ marginLeft: '2rem' }}>
                {
                    Object.keys(employeesPerArea).map((jobArea, index) => {
                        const jobAreaEmployeesCount = Object.keys(employeesPerArea[jobArea]).length;
                        let projectsCount = 0;

                        projects.forEach((project) => {
                            employeesPerArea[jobArea].forEach((employee) => {
                                if (project.employeesId.includes(employee.id)) {
                                    projectsCount++;
                                }
                            })
                        })

                        return (
                            <React.Fragment key={index}>
                                <JobArea
                                    jobArea={jobArea}
                                    jobAreaEmployeesCount={jobAreaEmployeesCount}
                                    projectsCount={projectsCount}
                                />
                                <div style={{ marginLeft: '1rem' }}>
                                    {(employeesPerArea[jobArea] || []).map((employee) => (
                                        <EmployeeContainer
                                            key={employee.id}
                                            id={employee.id}
                                            companyId={id}
                                            projects={projects}
                                            firstName={employee.firstName}
                                            lastName={employee.lastName}
                                            dateOfBirth={employee.dateOfBirth}
                                            jobTitle={employee.jobTitle}
                                            jobType={employee.jobType}
                                        />
                                    ))
                                    }
                                </div>
                            </ React.Fragment>
                        )
                    })
                }
            </div>
        </Box>
    </div>
)

Company.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    address: PropTypes.string,
    projects: PropTypes.array,
    employeesPerArea: PropTypes.object,
    showMore: PropTypes.bool,
    eventHandler: PropTypes.func
};
