import React, { useState } from 'react';
import { PropTypes } from 'prop-types';
import { ExpandMore, ChevronRight } from '@material-ui/icons';


export const Toggle = ({ show, eventHandler, children }) => {
    const [shouldShow, setShouldShow] = useState(show);

    console.log('test', children);
    return (
        <div className="icon-text-wrapper" onClick={() => { setShouldShow(!shouldShow); eventHandler() }}>
            {shouldShow
                ? <ExpandMore />
                : <ChevronRight />
            }
            <div className="children-wrapper">
                {children}
            </div>
        </div>
    )
}

Toggle.propTypes = {
    show: PropTypes.bool,
    eventHandler: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ])
};