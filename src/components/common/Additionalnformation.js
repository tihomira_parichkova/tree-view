import React from 'react';
import { PropTypes } from 'prop-types';

export const AdditionalInfo = ({ field, value }) => {
    return (
        <div>
            <b>{`${field}: `}</b>
            {value}
        </div>
    )
}

AdditionalInfo.propTypes = {
    field: PropTypes.string,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
};
